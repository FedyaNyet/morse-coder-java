this is the exact same implementation of my existing JavaScript project that wasn't quite fast enough, (https://bitbucket.org/FedyaNyet/morse-coder).

Here is a Java implementation that satisfies the time constrain.

Import this project into a java environment and run the main function located in Decoder to see the result of removing:
`-*--_---_-**_*-` and `*-**_*_**_*-` from `-_****_*___***_-_*-_*-*___*--_*-_*-*_***___***_*-_--*_*-`.
		
		