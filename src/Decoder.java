import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class Decoder{

	String message;
	String MATCH_CHAR = "X";
	
	public Decoder(String message){
		this.message = message;
	}
	/**
	 * Iteratively find all the results of removing one string from another.
	 * @param  {String} message The subject string from which `remove` will be removed
	 * @param  {String} remove  The string to remove from 'message'
	 * @return {array}         array of unique strings resulted from removing `remove` from `message`
	 */
	public HashMap<String, Boolean> itterative_remove(String message, String remove){

		//dictionary to store unique strings
		HashMap<String, Boolean> complete = new HashMap<String, Boolean>(); 

		//stack to keep track of iterations, with first iteration supplied.
		ArrayList<String[]> stack = new ArrayList<String[]>();
		String[] initial = {message, remove};
		stack.add(initial);
		while(!stack.isEmpty()){

			//pop the tuple to be worked on.
			String[] tup = stack.remove(stack.size()-1);
			String _message = tup[0];
			String _remove = tup[1];

			//if the tuple was completed, remove the x's and store it in the complete dictionary
			if(_remove.replace(MATCH_CHAR, "").length() == 0){
				String cleaned = _message.replace(MATCH_CHAR, "");
				complete.put(cleaned, true);
				continue;
			}

			//mark the next matching characters, and push the resulting string for later processing
			int r = _remove.lastIndexOf(MATCH_CHAR)+1;
			for(int m=_message.lastIndexOf(MATCH_CHAR)+1; m<_message.length(); m++){
				if(_message.charAt(m) != _remove.charAt(r))
					continue;

				//place an X in the matching character, and store it.
				String _m = _message.substring(0,m) + MATCH_CHAR + _message.substring(m+1);
				String _r = _remove.substring(0,r) + MATCH_CHAR + _remove.substring(r+1);
				String[] next = {_m,_r};
				stack.add(next);
			}
		}
		return complete;
	}

	/**
	 * (Public) Remove two mores-code strings from the mores-code message attribute.
	 * @param  {String} remove1 First string to be removed from the message
	 * @param  {String} remove2 Second string to be removed from the message
	 * @return {Array}         The unique sequences produces by removed both sequences from the message
	 */
	public Set<String> remove(String remove1, String remove2){
		HashMap<String, Boolean> first_pass = itterative_remove(this.message, remove1);
		//return the remove results if remove2 wasn't supplied.
		if(remove2.isEmpty())
			return first_pass.keySet();
		//build dictionary of unique results of removing `remove2` from every result of removing `remove1` from `message`
		HashMap<String, Boolean> ret = new HashMap<String, Boolean>();
		for(String first_message : first_pass.keySet()){
			HashMap<String, Boolean> second_pass = itterative_remove(first_message, remove2);
			ret.putAll(second_pass);
		}
		//return the unique strings.
		return ret.keySet();
	}
	
	public static void main(String[] args){
		Decoder decoder = new Decoder("-_****_*___***_-_*-_*-*___*--_*-_*-*_***___***_*-_--*_*-");
		long startTime = System.nanoTime();
		Set<String> result = decoder.remove("-*--_---_-**_*-", "*-**_*_**_*-");
		long endTime = System.nanoTime();

		long duration = (endTime - startTime)/1000000/1000;//seconds
		System.out.println(duration+ " Seconds");
		System.out.println(result.size());
		
	}
}
	
	